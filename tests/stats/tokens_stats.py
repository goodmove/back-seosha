
import unittest

from src.stats.tokens_stats import TokensStats
from src.filters.tokens_filter import TokensFilter
from src.dto.stats import TokensStatsDto

from nltk.tokenize import RegexpTokenizer


class TokensStatsTest(unittest.TestCase):

    def setUp(self):
        self.__tokenizer = RegexpTokenizer(r'\w+')
        self.__raw_text = 'Текст, который сейчас я буду тестировать. Некоторые слова должны быть выброшены. ' \
                          'Несколько слов в одном падеже. Радость, радуюсь, радость, радовался.'

    def test_tokens_stats(self):
        tokens = self.__tokenizer.tokenize(self.__raw_text)
        filtered_tokens = TokensFilter().filter_tokens_list(tokens)
        print(filtered_tokens)
        stats = TokensStats(tokens=filtered_tokens)
        print(TokensStatsDto(stats).serialize())

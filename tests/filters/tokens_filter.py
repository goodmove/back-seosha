
import unittest

from src.filters.tokens_filter import TokensFilter
from src.filters.stop_words.ru.yandex_stop_words_ru import YANDEX_STOP_WORDS_RU


class TokenFilterTest(unittest.TestCase):

    def setUp(self):
        self.__false_symbol = 'в'
        self.__true_symbol = 'пример'
        self.__test_tokens = [
            'в', 'на', 'предметная', 'область'
        ]
        self.__expected_test_tokens = [
            'предметная', 'область'
        ]
        self.__nltk_filter = TokensFilter()
        self.__yandex_filter = TokensFilter(stop_words=YANDEX_STOP_WORDS_RU)

    def __test_filter(self, input_filter: TokensFilter):
        self.assertListEqual(self.__expected_test_tokens, input_filter.filter_tokens_list(self.__test_tokens))
        self.assertTrue(input_filter.filter_token(self.__true_symbol))
        self.assertFalse(input_filter.filter_token(self.__false_symbol))

    def test_yandex_filter(self):
        self.__test_filter(self.__yandex_filter)

    def test_nltk_filter(self):
        self.__test_filter(self.__nltk_filter)
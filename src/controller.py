from requests.exceptions import ConnectionError

from src.dto.lemma import LemmaDto
from src.dto.site_data import SiteDataDto
from src.dto.site_statistic import SiteStatisticDto
from src.filters.tokens_filter import TokensFilter
from src.html.html_parser import HtmlParser
from src.html.errors import HtmlParsingException
from src.stats.tokens_stats import TokensStats
from src.tokenizer.tokenizer import Tokenizer
from src.api.errors import InvalidUrlError, AnalysisError

class RequestHandler:

	def __init__(self):
		self.__tokenizer = Tokenizer();
		self.__token_filter = TokensFilter()
    
	def __ensure_protocol_exists(self, url):
		if url.startswith("http://") or url.startswith("https://"):
			return url
		else:
			return ("http://" + url)

	def analyze_site(self, site_data: SiteDataDto) -> SiteStatisticDto:
		parser = None
		try:
			parser = HtmlParser(self.__ensure_protocol_exists(site_data.url))
		except UrlValidationError:
			raise InvalidUrlError(url)

		key_words = set() if site_data.keys is None else set(site_data.keys.split())

		try:
			all_tokens_count = len(self.__tokenizer.tokenize(parser.get_all_text()))
			filtered_tokens = self.__token_filter.filter_tokens_list(self.__tokenizer.tokenize(parser.get_important_text()))
			token_stats = TokensStats(filtered_tokens).stats
			lemmas_info = list([LemmaDto(
				token_info.normal_form,
				token_info.normal_form_occurrence_count(),
				token_info.normal_form in key_words or any([form in key_words for form in token_info.forms]),
				len(token_info.forms),
				[{ "word": form.token, "count": form.count } for form in token_info.forms.values()],
				) for token_info in token_stats])

			return SiteStatisticDto(
				site_data.url,
				all_tokens_count,
				len(token_stats),
				parser.get_title(),
				parser.get_meta_description(),
				parser.get_meta_keywords(),
				lemmas_info
			)
		except (HtmlParsingException, ConnectionError) as e:
			print(e)
			raise AnalysisError()


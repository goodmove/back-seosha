import requests
import bs4
from urllib.parse import urlparse

from src.html.errors import HtmlParsingException, UrlValidationError


class HtmlParser:
    important_tags = ["em", "i", "strong", "bold", "mark", "dfn", "h1", "h2", "h3", "h4", "h5", "h6"]

    def __init__(self, url, add_http_if_empty=True):
        try:
            parsed_url = urlparse(url)
            if add_http_if_empty and parsed_url.scheme == "":
                parsed_url = urlparse("http://" + url)
        except ValueError:
            raise UrlValidationError("Error in url")
        self.__url = parsed_url.geturl()
        self.__soup_parser = None

    def __lazy_parse_html(self):
        if (self.__soup_parser is None):
            url = self.__url
            response = requests.get(url)
            if response.status_code != 200:
                raise HtmlParsingException("Failed to fetch content from {}".format(url))
            self.__soup_parser = bs4.BeautifulSoup(response.text, "html.parser")

    def get_url(self):
        return self.__url

    def get_all_text(self):
        self.__lazy_parse_html()
        return self.__soup_parser.getText()

    def get_title(self):
        self.__lazy_parse_html()
        title = self.__soup_parser.title
        if title is not None:
            return title.getText()
        else:
            return None

    def get_important_text(self, include_meta=True):
        self.__lazy_parse_html()
        strings = []

        if include_meta:
            important_methods = [self.get_title, self.get_meta_keywords, self.get_meta_description]
            for method in important_methods:
                res = method()
                if res is not None:
                    strings.append(res)

        for tag in self.important_tags:
            for s in self.__soup_parser.find_all(tag):
                strings.append(s.getText())

        return " ".join(strings)

    def get_meta_keywords(self):
        self.__lazy_parse_html()

        keyword_tag = self.__soup_parser.find("meta", attrs={'name': 'Keywords'})
        if keyword_tag is not None:
            return keyword_tag["content"]
        return None

    def get_meta_description(self):
        self.__lazy_parse_html()

        description_tag = self.__soup_parser.find("meta", attrs={'name': 'description'})
        if description_tag is not None:
            return description_tag["content"]
        return None

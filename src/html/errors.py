class HtmlParsingException(Exception):

    def __init__(self, message):
        super().__init__(message)
        self.message = message


class UrlValidationError(Exception):

    def __init__(self, message):
        super().__init__(message)
        self.message = message

class SearchQueryDto(object):
    def __init__(self, search_query, aggregate_function='MAX'):
        self.search_query = search_query
        self.aggregate_function = aggregate_function

    @staticmethod
    def create_from_data(data):
        return SearchQueryDto(
            search_query=data['searchQuery'],
            aggregate_function=data.get('aggregateFunction')
        )

    def serialize(self):
        return {
            'searchQuery': self.search_query,
            'aggregateFunction': self.aggregate_function,
        }

    def __repr__(self):
        return "SearchQueryDto({0}, {1})".format(self.search_query, self.aggregate_function)

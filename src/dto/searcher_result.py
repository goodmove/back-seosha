from src.dto.lemma import LemmaDto
from src.dto.site_statistic import SiteStatisticDto


class SearcherResultDto:
    def __init__(self, words, details):
        self.words = words
        self.details = details

    def serialize(self):
        return {
            "summary": {
                "words": [w.serialize() for w in self.words]
            },

            "details": [d.serialize() for d in self.details]
        }

    @staticmethod
    def get_default():
        return SearcherResultDto(
            [
                LemmaDto("Дом", 200, False, 3, [{"word": "Домик", "count": 20}, {"word": "Домище", "count": 30}])
            ],
            [    
                SiteStatisticDto.get_default()  
            ]
        )

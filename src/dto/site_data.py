class SiteDataDto(object):
    def __init__(self, url, keys=''):
        self.url = url
        self.keys = keys

    @staticmethod
    def create_from_data(data):
        return SiteDataDto(
            url=data['url'],
            keys=data.get('keys')
        )

    def serialize(self):
        return {
            'url': self.url,
            'keys': self.keys,
        }

    def __repr__(self):
        return "SiteDataDto({0}, {1})".format(self.url, self.keys)

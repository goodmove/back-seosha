
from src.stats.occurrence_counts import TokenOccurrenceCount, TokenFormsOccurrenceCount


class TokenOccurrenceCountDto:

    def __init__(self, token_occurrence_count: TokenOccurrenceCount):
        self.__token_occurrence_count = token_occurrence_count

    def serialize(self):
        return {
            'token': self.__token_occurrence_count.token,
            'count': self.__token_occurrence_count.count
        }


class TokenFormsOccurrenceDto:

    def __init__(self, token_forms_occurrence_count: TokenFormsOccurrenceCount):
        self.__token_forms_occurrence_count = token_forms_occurrence_count

    def serialize(self):
        result = []
        forms = []
        for form in self.__token_forms_occurrence_count.forms.values():
            forms.append(TokenOccurrenceCountDto(form).serialize())
        result.append({
            'normal_form': TokenOccurrenceCountDto(self.__token_forms_occurrence_count.normal_form).serialize(),
            'abnormal_forms': forms,
            'all_forms_count': self.__token_forms_occurrence_count.all_forms_count(),
            'abnormal_forms_count': self.__token_forms_occurrence_count.abnormal_forms_count
        })

        return result

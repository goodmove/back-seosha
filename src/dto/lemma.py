class LemmaDto(object):
    def __init__(self, lemma, count, is_key_word, forms_count, same_root):
        self.lemma = lemma
        self.count = count
        self.is_key_word = is_key_word
        self.forms_count = forms_count
        self.same_root = same_root

    def serialize(self):
        return {
            "lemma": self.lemma,
            "count": self.count,
            "isKeyWord": self.is_key_word,
            "formsCount": self.forms_count,
            "sameRoot": self.same_root
        }

from src.dto.lemma import LemmaDto


class SiteStatisticDto(object):
    def __init__(self, url, word_count, distinct_lemma_count, title, description, key_words, lemmas):
        self.url = url
        self.word_count = word_count
        self.distinct_lemma_count = distinct_lemma_count
        self.title = title
        self.description = description
        self.key_words = key_words
        self.lemmas = lemmas

    def serialize(self):
        return {
            "url": self.url,
            "wordCount": self.word_count,
            "distinctLemmaCount": self.distinct_lemma_count,
            "title": self.title,
            "description": self.description,
            "keyWords": self.key_words,
            "lemmas": [l.serialize() for l in self.lemmas]
        }

    @staticmethod
    def get_default():
        return SiteStatisticDto(
            "https://someurl.com",
            1200,
            500,
            "Title",
            "Description",
            "some words keywords",
            [
                LemmaDto("Дом", 200, False, 3, [{"word": "Домик", "count": 20}, {"word": "Домище", "count": 30}])
            ]
        )
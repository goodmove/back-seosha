

from src.dto.occurrence_counts import TokenFormsOccurrenceDto, TokenOccurrenceCountDto
from src.stats.tokens_stats import TokensStats


class TokensStatsDto:

    def __init__(self, tokens_stats: TokensStats):
        self.__tokens_stats = tokens_stats

    def serialize(self):
        result = []
        for stat in self.__tokens_stats.stats.values():
            result.append([
                TokenFormsOccurrenceDto(stat).serialize()
            ])

        return result

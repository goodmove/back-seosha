class string(str):
    def filter(self, condition):
        return string("".join(c for c in self if condition(c)))


class Tokenizer():
    def __init__(self):
        self.__delimiters = set(''' ,:\'!.@#$%^&*()+;/|[]<>?=–—_''')
        self.__ignored_chars = set('1234567890}{`"')
        self.__illegal_tokens = set('-–—')

    def tokenize(self, input_string):
        tokens = []
        start, end = 0, 0

        def append_token(token):
            token = string(token.strip()).filter(lambda x: x not in self.__ignored_chars)
            if len(token) > 0 and token not in self.__illegal_tokens:
                tokens.append(token)    

        for char in input_string:
            if char in self.__delimiters:
                token = input_string[start:end]
                start = end + 1
                append_token(token)
            end += 1

        append_token(input_string[start:end])

        return tokens

from flask import Flask

from src.api.api import init_api
from src.controller import RequestHandler

if __name__ == '__main__':
	app = Flask(__name__)
	handler = RequestHandler()
	init_api(app, handler)
	app.run(host='localhost', port=8070, debug=False)

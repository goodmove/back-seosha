
import typing


class TokenOccurrenceCount:

    def __init__(self, count: int=0, token: str=''):
        self.__count = count
        self.__token = token

    @property
    def token(self):
        return self.__token

    @property
    def count(self):
        return self.__count

    @count.setter
    def count(self, other: int):
        self.__count = other

    def increase_count(self):
        self.__count += 1
        return self


class TokenFormsOccurrenceCount:

    def __init__(
            self,
            normal_form_count: int=0,
            normal_form_token: str='',
            forms: typing.Dict[str, TokenOccurrenceCount]=None
    ):
        self.__normal_form_count = TokenOccurrenceCount(count=normal_form_count, token=normal_form_token)
        if forms is None:
            forms = dict()

        self.__forms = forms
        self.__abnormal_forms_count = 0

    @property
    def forms(self):
        return self.__forms

    @property
    def normal_form(self):
        return self.__normal_form_count.token

    @property
    def abnormal_forms_count(self):
        return self.__abnormal_forms_count

    def normal_form_occurrence_count(self):
        return self.__normal_form_count.count

    def all_forms_count(self):
        return self.normal_form_occurrence_count() + self.abnormal_forms_count

    def increase_form_occurrence(self, form_token: str):
        if form_token == self.__normal_form_count.token:
            self.__normal_form_count.increase_count()
            return

        self.__forms[form_token] = self.__forms.get(form_token, TokenOccurrenceCount(token=form_token)).increase_count()
        self.__abnormal_forms_count += 1

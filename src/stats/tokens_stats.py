
import typing

from pymorphy2 import MorphAnalyzer

from src.stats.occurrence_counts import TokenFormsOccurrenceCount


class TokensStats:

    def __init__(self, tokens: typing.List[str]=None):
        self.__analyser = MorphAnalyzer()

        if tokens is not None:
            self.__tokens = tokens
        else:
            self.__tokens = []

        self.__init_stats()

    def __init_stats(self):
        self.__stats = dict()

        for token in self.__tokens:
            normal_forms = set()
            token = token.lower()
            for token_parsed in self.__analyser.parse(token):
                normal_form = token_parsed.normal_form
                normal_forms = normal_forms.union({normal_form})
            for normal_form in normal_forms:
                normal_form_stats = self.__stats.get(normal_form, None)
                if normal_form_stats is None:
                    self.__stats[normal_form] = TokenFormsOccurrenceCount(normal_form_token=normal_form)
                self.__stats[normal_form].increase_form_occurrence(token)

    @property
    def stats(self):
        return self.__stats.values()

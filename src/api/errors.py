class ApiError(Exception):

	def __init__(self, code: str, message: str):
		self.__code = code
		self.__message = message

	def as_json(self) -> str:
		return {
			'result': 'error',
			'code': self.__code,
			'message': self.__message,
		}


class InvalidUrlError(ApiError):

	def __init__(self, url: str):
		super().__init__('INVALID_URL', 'Invalid url: {}'.format(url))

class AnalysisError(ApiError):

	def __init__(self):
		super().__init__('ANALYSIS_ERROR', 'Failed to perform website analysis')

class RequiredField(ApiError):

	def __init__(self, field: str):
		super().__init__('REQUIRED', 'Field {} is required'.format(field))

from flask import make_response
from flask import jsonify
from flask import request
from flask import abort

from src.dto.searcher_result import SearcherResultDto
from src.dto.site_data import SiteDataDto
from src.dto.search_query import SearchQueryDto
from src.dto.site_statistic import SiteStatisticDto
from src.api.errors import ApiError, RequiredField


def init_api(app, request_handler):
	app.after_request(allow_any_cors)

	@app.route('/api/analysis/site', methods=['POST'])
	def analyse_site():
		if not request.is_json:
			abort(400)		
		
		try:
			request_data = SiteDataDto.create_from_data(request.get_json())
			response_data = request_handler.analyze_site(request_data)
			return make_response(jsonify(response_data.serialize()), 200)
		except KeyError as err:
			return make_response(jsonify(RequiredField(err.args[0]).as_json()), 400)
		except ApiError as err:
			return make_response(jsonify(err.as_json()), 400)


	@app.route('/api/analysis/query', methods=['POST'])
	def analyse_searcher_answer():
	    if not request.is_json:
	        abort(400)

	    request_data = SearchQueryDto.create_from_data(request.get_json())
	    print(request_data)

	    return make_response(jsonify(SearcherResultDto.get_default().serialize()), 200)


def allow_any_cors(response):
	response.headers['Access-Control-Allow-Origin'] = '*'
	if request.method == 'OPTIONS':
		response.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT'
		headers = request.headers.get('Access-Control-Request-Headers')
		if headers:
			response.headers['Access-Control-Allow-Headers'] = headers
	return response


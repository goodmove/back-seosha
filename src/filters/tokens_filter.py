
import typing

from src.filters.stop_words.ru.nltk_stop_words_ru import NLTK_STOP_WORDS_RU


class TokensFilter:

    TOKEN_MIN_LENGTH = 3

    def __init__(self, stop_words: typing.List[str]=None):
        if stop_words is not None:
            self.__stop_words = stop_words
        else:
            self.__stop_words = NLTK_STOP_WORDS_RU

    def filter_token(self, token: str) -> bool:
        return token not in self.__stop_words and len(token) >= self.TOKEN_MIN_LENGTH

    def filter_tokens_list(self, tokens: typing.List[str]=None) -> typing.List[str]:
        result = []
        if tokens is None:
            return result

        result = list(filter(self.filter_token, tokens))

        return result
